const TimeFormat = "Y-m-d H:i:s";
const DateFormat = "Y-m-d";
const Int = 'int';
const String = 'string';
const Array = '[]';
const Bool = 'bool';
const Field = {
	field: String,				//поле не прошедшее валидацию
	message: String,			//текс ошибки
	extra: Array,				//дополнительные данные"
};
const Notification = {
	type_enum: Int, 			//тип уведомления",
	message: String, 			//текст уведомления",
	extra: Array,				//дополнительные данные"
};