## API location: 

https://walk2.top

---

## Controllers:

[User](user)

---

## Request Required headers: 
```javascript
let lang = "EN"; //Язык интерфейса для устройства
const Http = new XMLHttpRequest();
Http.setRequestHeader("Accept-Language", lang);								//Язык пользователя
Http.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
Http.setRequestHeader("X-Requested-With", "XMLHttpRequest");
```

## Request Optional headers: 
```javascript
let token = "1542722187-AB09FA0BCE010F..."; //Токен идентификации устройства
const Http = new XMLHttpRequest();
Http.setRequestHeader("Authorization", "Bearer " + token)					//Токен выданный после логина;
```

---

## Response Format:

[Типы данных](baseType.js)

--- 

**STATUS_CODE_OK** [200] Все прошло хорошо: 
```javascript
const response = {
	status: Bool,					//true
	data: Array,					//В случае успеха содержит ответ от API (null - если ответ пустой)
	notifications: [Notification]	//[Optional] Уведомления для пользователя
}
```

---

**STATUS_PRECONDITION_FAILED** [412] Сервер запретил дальнейшее выполнение: 

**STATUS_CODE_REDIRECT** [302] Пользователь должен быть пененаправлен:
```javascript
const response = {
	status: Bool, 					//false
	notifications: [Notification]	//[Optional] Уведомления для пользователя
}
```

---

**STATUS_CODE_LOGOUT** [401] Пользователю нужно войти заново: 

**STATUS_CODE_FORBIDDEN** [403] В доступе отказанно: 

**STATUS_CODE_NOT_FOUND** [404] Сущность или URL не найдена: 

```javascript
const response = {
	status: Bool 			//false
}
```

---

**STATUS_CODE_VALIDATION** [422]  Пользователь ввел неверные данные: 

```javascript
const response = {
	status: Bool, 			//false
	fields: [Field]			//Список полей не прошедших валидацию
}
```

---

**STATUS_CODE_SERVER_ERROR** [500] Неожиданная ошибка на сервере: 
```javascript
const response = {
	status: Bool, 			//false",
	message: String,		//[Debug] Текст ошибки",
	extra: [String] 		//[Debug] Дополнительные данные"
}
```

