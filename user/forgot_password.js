const API = [
	{
		method: "POST",
		route: "/user/forgot-password",
		description: "Форма восстановления пароля",
		role_enum: [
			ROLE_GUEST
		],
		input: {
			email: String
		},
		output: {}
	}
];