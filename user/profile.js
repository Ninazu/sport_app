const ContactType = {
	/**
	 * Айди контакта
	 * @use constant.js
	 */
	user_contact_id: Int,
	/**
	 * Тип контакта
	 * @use constant.js
	 */
	type_enum: Int,
	/**
	 * Значение
	 * @len = 3-254
	 */
	value: String
};

const ProfileType = {
	/**
	 * Роль
	 * @use constant.js
	 * @readonly
	 */
	role_enum: Int,
	/**
	 * Статус
	 * @use constant.js
	 * @readonly
	 */
	status_enum: Int,
	/**
	 * Email
	 * @len = 5-254
	 */
	email: String,
	/**
	 * Ник в системе
	 * @len = 5-50
	 */
	name: String,
	/**
	 * Пол
	 * @use constant.js
	 * @optional null
	 */
	gender_enum: Int,
	/**
	 * Дата рождения
	 * @optional null
	 */
	birthday: Date,
	/**
	 * Список контактов
	 * @optional null
	 */
	contacts: [ContactType]
};

const API = [
	{
		method: "GET",
		route: "/user/profile",
		description: "Получить данные по профайлу",
		role_enum: [
			ROLE_USER,
			ROLE_COACH
		],
		status_enum: [
			STATUS_ACTIVE,
			STATUS_NOT_CONFIRMED
		],
		input: {},
		output: ProfileType
	},
	{
		method: "POST",
		route: "/user/profile",
		description: "Сохранить профайл",
		role_enum: [
			ROLE_USER,
			ROLE_COACH
		],
		status_enum: [
			STATUS_ACTIVE,
			STATUS_NOT_CONFIRMED
		],
		input: ProfileType,
		output: {
			/**
			 * Роль текущего пользователя
			 * @use constant.js
			 */
			role_enum: Int,
			/**
			 * Стутус текущего пользователя
			 * @use constant.js
			 */
			status_enum: Int
		}
	}
];

