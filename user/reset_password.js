const API = [
	{
		method: "POST",
		route: "/user/reset-password",
		description: "Форма сброса старого пароля",
		role_enum: [
			ROLE_GUEST
		],
		input: {
			/**
			 * Секретный код из письма
			 */
			pin: Int,
			/**
			 * Новый пароль
			 * @len = 8-50
			 */
			password: String
		},
		output: {}
	}
];