const API = [
	{
		method: "POST",
		route: "/user/login",
		description: "Регистрация и авторизация пользователя",
		role_enum: [
			ROLE_GUEST
		],
		input: {
			/**
			 * Логин
			 */
			email: String,
			/**
			 * Пароль
			 * @len = 8-50
			 */
			password: String,
			/**
			 * MD5 хеш уникального идентификатора устройства
			 * @len = 32
			 */
			device_hash: String
		},
		output: {
			/**
			 * Идентификатор пользователя
			 */
			user_id: Int,
			/**
			 * Роль
			 */
			role_enum: Int,
			/**
			 * Статус
			 */
			status_enum: Int,
			/**
			 * Ник в системе
			 */
			name: String,
			/**
			 * Токен для идентификации устройства
			 */
			token: String,
		}
	},
	{
		method: "POST",
		route: "/user/exist",
		description: "Проверить существование пользователя",
		role_enum: [
			ROLE_GUEST
		],
		input: {
			/**
			 * Логин
			 */
			email: String
		},
		output: {
			/**
			 * Это новый пользователь или нет
			 */
			is_new: Bool,
		}
	}
];