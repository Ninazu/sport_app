const API = [
	{
		method: "POST",
		route: "/user/logout",
		description: "Выйти из системы",
		role_enum: [
			ROLE_USER,
			ROLE_COACH
		],
		input: {
			/**
			 * Разлогиниться на всех устройствах
			 * @optional false
			 */
			all_devices: Bool
		},
		output: {}
	}
];